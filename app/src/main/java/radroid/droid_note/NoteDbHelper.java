package radroid.droid_note;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import radroid.droid_note.NoteDbContract.NoteEntry;

/**
 * Created by Admin on 16.09.15.
 */
public class NoteDbHelper extends SQLiteOpenHelper {

	public static final int DATABASE_VERSION = 4;
	public static final String DATABASE_NAME = "notes.db";

	NoteDbHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		final String SQL_CREATE_TABLE = "CREATE TABLE " + NoteEntry.TABLE_TITLE + "( "
				+ NoteEntry._ID + " INTEGER PRIMARY KEY, "
				+ NoteEntry.NOTE_COLUMN + " TEXT,"
				+ NoteEntry.MOD_DATE_COLUMN + " TEXT NOT NULL"
				+ ");";

		db.execSQL(SQL_CREATE_TABLE);

	}


	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + NoteEntry.TABLE_TITLE);
		onCreate(db);
	}


}












