package radroid.droid_note;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

/**
 * A placeholder fragment containing a simple view.
 */
public class EditActivityFragment extends Fragment {

	View rootView;
	long rowId;
	NoteDbContract noteDbContract = new NoteDbContract();
	TextView textView;

	public EditActivityFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_edit, container, false);
		EditText editText = (EditText) rootView.findViewById(R.id.text_edit_edit_fragment);
		textView = (TextView) rootView.findViewById(R.id.text_view_mod_date);
		Intent intent = getActivity().getIntent();

		if (intent != null && intent.hasExtra("row_id")) {

			rowId = intent.getLongExtra("row_id", -1);
		}

		if (intent != null && intent.hasExtra(Intent.EXTRA_TEXT)) {

			String note = intent.getStringExtra(Intent.EXTRA_TEXT);
			editText.setText(note);
		}

		if (intent != null && intent.hasExtra("mod_date")) {

			String modDateData = intent.getStringExtra("mod_date");
			textView.setText("Last modified  " + modDateData);
		}

		editText.setSelection(editText.getText().length());


		return rootView;
	}


	@Override
	public void onPause() {
		EditText editText = (EditText) rootView.findViewById(R.id.text_edit_edit_fragment);
		String noteData = editText.getText().toString();
		if (noteData.length() != 0) {
			String modDateData = noteDbContract.getModDate();

			if (rowId == -1) {
				throw new UnsupportedOperationException("Cannot get row id");
			}


			ContentValues contentValues = new ContentValues();
			contentValues.put(NoteDbContract.NoteEntry.NOTE_COLUMN, noteData);
			contentValues.put(NoteDbContract.NoteEntry.MOD_DATE_COLUMN, modDateData);

			textView.setText("Last modified  " + modDateData);

			getActivity().getContentResolver().update(NoteDbContract.buildUriWithId(rowId), contentValues, null, null);

		} else {

			getActivity().getContentResolver().delete(NoteDbContract.NoteEntry.CONTENT_URI, NoteDbContract.NoteEntry._ID + " = " + rowId, null);
		}
		super.onPause();
	}
}
