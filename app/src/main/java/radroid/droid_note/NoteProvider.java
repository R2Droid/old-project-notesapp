package radroid.droid_note;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import java.sql.SQLException;

import radroid.droid_note.NoteDbContract.NoteEntry;

/**
 * Created by Admin on 18.09.15.
 */
public class NoteProvider extends ContentProvider {
	Context context;
	private static final int NOTE_LIST = 1;
	private static final int NOTE_ID = 2;
	private static final UriMatcher sUriMatcher;

	static {

		sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

		sUriMatcher.addURI(NoteDbContract.CONTENT_AUTHORITY, "notes", NOTE_LIST);
		sUriMatcher.addURI(NoteDbContract.CONTENT_AUTHORITY, "notes/#", NOTE_ID);
	}

	private SQLiteOpenHelper mSQLiteOpenHelper;

	@Override
	public boolean onCreate() {
		mSQLiteOpenHelper = new NoteDbHelper(getContext());
		context = getContext();

		return true;
	}

	public String getType(@NonNull Uri uri) {
		String type;
		switch (sUriMatcher.match(uri)) {

			case NOTE_LIST:
				type = NoteEntry.CONTENT_DIR_TYPE;
				break;
			case NOTE_ID:
				type = NoteEntry.CONTENT_ITEM_TYPE;
				break;
			default:
				throw new UnsupportedOperationException("Unknown type for uri: " + uri);
		}
		return type;
	}

	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
		Cursor retCursor;

		switch (sUriMatcher.match(uri)) {
			case NOTE_ID:
				retCursor = mSQLiteOpenHelper.getReadableDatabase().query(NoteEntry.TABLE_TITLE, projection, NoteEntry._ID + " = " + uri.getLastPathSegment(), selectionArgs, null, null, sortOrder);
				break;
			case NOTE_LIST:
				retCursor = mSQLiteOpenHelper.getReadableDatabase().query(NoteEntry.TABLE_TITLE, projection, selection, selectionArgs, null, null, sortOrder);
				break;
			default:
				throw new UnsupportedOperationException("Query operation failed - unknown uri:" + uri);
		}
		if (retCursor != null) {
			retCursor.setNotificationUri(context.getContentResolver(), uri);
		} else {
			throw new UnsupportedOperationException("Query operation failed - retCursor is null - uri:" + uri);
		}
		return retCursor;
	}

	public Uri insert(@NonNull Uri uri, ContentValues contentValues) {
		long insertId;
		Uri retUri;


		insertId = mSQLiteOpenHelper.getWritableDatabase().insert(NoteEntry.TABLE_TITLE, NoteEntry._ID + " = " + uri.getLastPathSegment(), contentValues);

		if (insertId > 0) {
			retUri = ContentUris.withAppendedId(NoteEntry.CONTENT_URI, insertId);
		} else {
			throw new UnsupportedOperationException("Insert operation - failed" + uri);
		}
		context.getContentResolver().notifyChange(uri, null);
		return retUri;
	}

	@Override
	public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
		int retDeleted = 0;

		switch (sUriMatcher.match(uri)) {
			case NOTE_ID:
				retDeleted = mSQLiteOpenHelper.getWritableDatabase().delete(NoteEntry.TABLE_TITLE, NoteEntry._ID + " = " + uri.getLastPathSegment(), null);
				break;
			case NOTE_LIST:
				retDeleted = mSQLiteOpenHelper.getWritableDatabase().delete(NoteEntry.TABLE_TITLE, selection, null);
				break;
			default:
				throw new UnsupportedOperationException("Delete operation failed - unknown uri: " + uri);
		}


		if (retDeleted != 0) {
			context.getContentResolver().notifyChange(uri, null);
		} else {
			throw new UnsupportedOperationException("Delete operation failed - retDelete = 0 uri: " + uri);
		}
		return retDeleted;
	}

	@Override
	public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {

		int retRowsUpdated = 0;

		retRowsUpdated = mSQLiteOpenHelper.getWritableDatabase().update(NoteEntry.TABLE_TITLE, values, NoteEntry._ID + " = " + uri.getLastPathSegment(), null);
		if (retRowsUpdated != 0) {
			context.getContentResolver().notifyChange(uri, null);
		} else {
			throw new UnsupportedOperationException("Update operation failed - retRowsUpdate = 0 uri: " + uri);
		}

		return retRowsUpdated;
	}


}
