package radroid.droid_note;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Admin on 16.09.15.
 */
public class NoteDbContract {

	public static final String CONTENT_AUTHORITY = "radroid.droid_note.noteprovider";
	public static final Uri CONTENT_URI_BASE = Uri.parse("content://" + CONTENT_AUTHORITY);

	public static final String PATH_NOTE = "notes";


	public static final String DEFAULT_SORT_ORDER = NoteEntry.MOD_DATE_COLUMN + " DESC";


	public static abstract class NoteEntry implements BaseColumns {

		public static final Uri CONTENT_URI = CONTENT_URI_BASE.buildUpon().appendPath(PATH_NOTE).build();

		public static final String CONTENT_DIR_TYPE = "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_NOTE;
		public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_NOTE;

		public static final String TABLE_TITLE = "notes";
		public static final String NOTE_COLUMN = "note";
		public static final String MOD_DATE_COLUMN = "mod_date";

	}

	public String getModDate() {
		String retModDate = new SimpleDateFormat("yyyy-MM-dd, HH:mm:ss").format(new Date());
		SimpleDateFormat.getDateInstance();
		return retModDate;
	}

	public static Uri buildUriWithId(long id) {
		return ContentUris.withAppendedId(NoteEntry.CONTENT_URI, id);
	}

}
