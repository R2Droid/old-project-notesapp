package radroid.com.testing;


import java.util.Arrays;

/**
 * Created by rados on 17.11.2015.
 */
class Solution {
	public int solution(int[] A) {
		// write your code in Java SE 8

		long sum = 0;
		long arraySum = 0;
		long a = 0;

		if(A.length == 0){
			return -1;
		}

		for(int z = 0;z < A.length;z++){
			arraySum += A[z];
		}

		for(int i = 0;i < A.length; i++){
			if( (arraySum - (sum + A[i])) == arraySum){
				return i;
			}

			sum += (long) A[i];
			arraySum-=A[i];
		}
		return -1;

	}
}
