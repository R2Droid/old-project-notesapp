package radroid.droid_note;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.GridView;
import android.widget.SimpleCursorAdapter;

/**
 * Created by rados on 09.09.2015.
 */
public class FragmentMain extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
	public static final int LOADER_ID = 1;
	SimpleCursorAdapter simpleCursorAdapter;


	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View rootView = inflater.inflate(R.layout.fragment_main, container, false);


		final String[] mFromColumns = {NoteDbContract.NoteEntry.NOTE_COLUMN};
		final int[] mToLayout = {R.id.text_view_grid_item};

		simpleCursorAdapter = new SimpleCursorAdapter(rootView.getContext(), R.layout.grid_item_main, null, mFromColumns, mToLayout, 0);

		GridView gridView = (GridView) rootView.findViewById(R.id.grid_view_fragment_main);
		registerForContextMenu(gridView);
		gridView.setAdapter(simpleCursorAdapter);


		gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Cursor cursor = (Cursor) simpleCursorAdapter.getItem(position);
				String note = cursor.getString(cursor.getColumnIndex(NoteDbContract.NoteEntry.NOTE_COLUMN));
				Intent intent = new Intent(getActivity(), EditActivity.class).putExtra(Intent.EXTRA_TEXT, note);
				intent.putExtra("row_id", simpleCursorAdapter.getItemId(position));
				intent.putExtra("mod_date", cursor.getString(cursor.getColumnIndex(NoteDbContract.NoteEntry.MOD_DATE_COLUMN)));

				startActivity(intent);
			}
		});
		getLoaderManager().initLoader(LOADER_ID, null, this);


		return rootView;
	}


	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {


		return new CursorLoader(
				getActivity(),
				NoteDbContract.NoteEntry.CONTENT_URI,
				null,
				null,
				null,
				NoteDbContract.DEFAULT_SORT_ORDER
		);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		simpleCursorAdapter.changeCursor(data);
		Log.v("LOADER MANAGER: ", "FINISHED");

	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		simpleCursorAdapter.changeCursor(null);

	}


	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		if (v.getId() == R.id.grid_view_fragment_main) {
			String[] menuItems = getResources().getStringArray(R.array.context_menu_items_array);
			for (int i = 0; i < menuItems.length; i++) {
				menu.add(Menu.NONE, i, i, menuItems[i]);
			}
		}


	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
		long rowId = simpleCursorAdapter.getItemId(info.position);

		switch (item.getItemId()) {
			case 0:
				getActivity().getContentResolver().delete(NoteDbContract.buildUriWithId(rowId), null, null);
				return true;

			case 1:
				Log.v("CONTEXT MENU: ", "GETTING THE ITEM");

				Intent intent = new Intent(Intent.ACTION_SEND);
				Cursor cursor = (Cursor) simpleCursorAdapter.getItem(info.position);
				String note_extra = cursor.getString(cursor.getColumnIndex(NoteDbContract.NoteEntry.NOTE_COLUMN));

				intent.setType("text/plain");
				intent.putExtra(Intent.EXTRA_TEXT, note_extra);

				startActivity(intent);
				return true;

			default:
				Log.w("CONTEXT MENU", "SOMETHING WENT WRONG");
				return super.onContextItemSelected(item);
		}

	}
}















