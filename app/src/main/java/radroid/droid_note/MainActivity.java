package radroid.droid_note;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity
{
	NoteDbContract noteDbContract = new NoteDbContract();


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		getContentResolver();
		Button newNoteButton = (Button)findViewById(R.id.createNoteButton);

		newNoteButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				createNote();
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	public void createNote(){
		Intent intent = new Intent (this,EditActivity.class);
		ContentValues values = new ContentValues();
		String modDate = noteDbContract.getModDate();
		Uri uri;

		values.put(NoteDbContract.NoteEntry.NOTE_COLUMN, "");
		values.put(NoteDbContract.NoteEntry.MOD_DATE_COLUMN, modDate);

		uri = this.getContentResolver().insert(NoteDbContract.NoteEntry.CONTENT_URI, values);
		intent.putExtra("row_id", ContentUris.parseId(uri));
		intent.putExtra("mod_date", modDate);
		startActivity(intent);
	}

}
