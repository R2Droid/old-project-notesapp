package radroid.droid_note;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.test.AndroidTestCase;

import radroid.droid_note.NoteDbContract.NoteEntry;

/**
 * Created by Admin on 16.09.15.
 */
public class DatabaseTest extends AndroidTestCase {


	public void testCreateDatabase() {
		SQLiteOpenHelper sqLiteOpenHelper = new NoteDbHelper(getContext());
		SQLiteDatabase db = sqLiteOpenHelper.getReadableDatabase();

		assertTrue("Database not created", db.isOpen());
	}

	public void testInsert() {

		final String notesDummyData = "WHAAATTT!!!!???" +
				"You other brother cant deny ";
		final int modDateDummyData = 20151123;

		long insertId;

		SQLiteOpenHelper sqLiteOpenHelper = new NoteDbHelper(getContext());
		SQLiteDatabase db = sqLiteOpenHelper.getWritableDatabase();

		ContentValues cV = new ContentValues();


		cV.put(NoteEntry.NOTE_COLUMN, notesDummyData);
		cV.put(NoteEntry.MOD_DATE_COLUMN, modDateDummyData);

		insertId = db.insert(NoteEntry.TABLE_TITLE, null, cV);
		assertTrue("Data not inserted into database", insertId != -1);
	}


	public void testQuery() {
		final String notesDummyData = "I like big notes and I can not lie," +
				"You other brother cant deny ";
		final String modDateDummyData = "2015-01-22, 22:00:12";

		long insertId;


		SQLiteOpenHelper sqLiteOpenHelper = new NoteDbHelper(getContext());
		SQLiteDatabase db = sqLiteOpenHelper.getWritableDatabase();

		ContentValues cV = new ContentValues();

		cV.put(NoteEntry.NOTE_COLUMN, notesDummyData);
		cV.put(NoteEntry.MOD_DATE_COLUMN, modDateDummyData);

		insertId = db.insert(NoteEntry.TABLE_TITLE, null, cV);
		assertTrue("Data not inserted into database, query check", insertId != -1);


		Cursor cursor = db.query(NoteEntry.TABLE_TITLE, null, null, null, null, null, null);

		assertTrue("Cursor should not be empty, query check", cursor.moveToFirst());


	}


}
